#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    int new_x;
    int new_y;

    new_x = xc + x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    //7 points
	int new_x1;
	int new_y1;

	new_x1 = xc + y;
	new_y1 = yc + x;
	SDL_RenderDrawPoint(ren, new_x1, new_y1);

	int new_x2;
	int new_y2;

	new_x2 = xc + y;
	new_y2 = yc + -x;
	SDL_RenderDrawPoint(ren, new_x2, new_y2);

	int new_x3;
	int new_y3;

	new_x3 = xc + x;
	new_y3 = yc + -y;
	SDL_RenderDrawPoint(ren, new_x3, new_y3);

	int new_x4;
	int new_y4;

	new_x4 = xc + -x;
	new_y4 = yc + -y;
	SDL_RenderDrawPoint(ren, new_x4, new_y4);

	int new_x5;
	int new_y5;

	new_x5 = xc + -y;
	new_y5 = yc + -x;
	SDL_RenderDrawPoint(ren, new_x5, new_y5);

	int new_x6;
	int new_y6;

	new_x6 = xc + -y;
	new_y6 = yc + x;
	SDL_RenderDrawPoint(ren, new_x6, new_y6);

	int new_x7;
	int new_y7;

	new_x7 = xc + -x;
	new_y7 = yc + y;
	SDL_RenderDrawPoint(ren, new_x7, new_y7);
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int p = 1 - R;
	int x = 0;
	int y = R;
	Draw8Points(xc, yc, x, y, ren);
	while (x <= y)
	{
		if (p < 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			y = y - 1;
			p += 2 * (x - y) + 5;
		}
		x = x + 1;
		Draw8Points(xc, yc, x, y, ren);
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int p = 1 - R;
	int x = 0;
	int y = R;
	Draw8Points(xc, yc, x, y, ren);
	while (x <= y)
	{
		if (p < 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			y--;
			p += 2 * (x - y) + 5;
		}
		x++;
		Draw8Points(xc, yc, x, y, ren);
	}
}
